# Laravel contact manager app built with vue js made by mehdi hasanpour

this project is a contact manager which made by laravel in backend and vue in front-end

this project wrote by mehdi hasanpour in few days before 2020

## Getting Started

Clone the repository. The repository contains a Laravel project. You can run the project with this command:

```
php artisan serve
```

### Prerequisites

You need the following installed:

* [Node.js and NPM](https://nodejs.org/en/download/)
* [PHP](https://www.php.net/manual/en/install.php/)
* [Laravel](https://laravel.com/docs/5.8/installation)

## Built With

* [Laravel](https://laravel.com/) - Used to build the backend of the web app
* [Vue.js](https://vuejs.org/) - Used to frontend of the web app
* [sqlite](https://pusher.com/) - for storing contact

* this project supports rtl language 

* we use vazir font in this contact manager
